![FabLamp](https://framagit.org/fablab-renens/repos/raw/master/images/FabLamp/fablamp.png "Image FabLamp")
# [FabLamp](https://framagit.org/fablab-renens/FabLamp/wikis/home)
**Une petite lampe à construire soi-même dans un FabLab !**    
La documentation du projet est disponible dans le [wiki](https://framagit.org/fablab-renens/FabLamp/wikis/home).
