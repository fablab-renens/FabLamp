# [Électronique](https://framagit.org/fablab-renens/FabLamp/wikis/L'%C3%A9lectronique)
**FabLamp.ino >** Le programme Arduino de la lampe. À téléverser sur un ATtiny85 en suivant [ce guide](https://framagit.org/fablab-renens/Arduino-ATtiny/wikis/home).    
**Circuit.fzz >** Le fichier d'édition du circuit électronique. Généré avec [Fritzing](http://fritzing.org/home/).   
